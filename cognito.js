var https = require('https');
var fs = require('fs');
var AWS = require('aws-sdk');
var AWSCognito = require('amazon-cognito-identity-js-node');
var jwtDecode = require('jwt-decode');

var cognitoEndpoint = 'cognito-idp.us-east-1.amazonaws.com';
var region = 'us-east-1';
var userPoolId = 'us-east-1_64LHNwNZL';
var clientId = 'kbhshqd3mj3qd3cn243p402ak';
var identityPoolId = 'us-east-1:f9b91034-40a0-4e0d-894a-62046393501f';

var credentials = {
	"cognitoEndpoint" : cognitoEndpoint,
	"region" : region,
	"userPoolId" : userPoolId,
	"clientId" : clientId,
	"identityPoolId" : identityPoolId
}

var cognitoUser = null;

AWS.config.region = region;
AWS.config.credentials = new AWS.CognitoIdentityCredentials({ IdentityPoolId: identityPoolId });
AWS.config.update({ accessKeyId: 'anything', secretAccessKey: 'anything' });

var poolData = { UserPoolId: userPoolId, ClientId: clientId };
var userPool = new AWSCognito.CognitoUserPool(poolData);

var setupUser = function(username) {
  var userData = { Username : username, Pool : userPool };
  cognitoUser = new AWSCognito.CognitoUser(userData);
}


var signIn = function(username, password, res) {
  setupUser(username);

  var authenticationData = { Username: username, Password: password };
  var authenticationDetails =  new AWSCognito.AuthenticationDetails(authenticationData);

console.log(authenticationDetails);

  cognitoUser.authenticateUser(authenticationDetails, {
     onSuccess: function (result) {
      // console.log('access token + ' + result.getAccessToken().getJwtToken());
       console.log(result.getAccessToken().getJwtToken());
       console.log("ID TOKEN")
       console.log(result.idToken.jwtToken);
       var decoded = jwtDecode(result.idToken.jwtToken);
       res.end(JSON.stringify({"idToken": result.idToken.jwtToken,
           "accessToken": result.getAccessToken().getJwtToken(),
           "refreshToken": result.getRefreshToken().getToken(), "error": ''})); return;
     },
       onFailure: function(err) {
         console.log("login failed?");
	 res.end(JSON.stringify({result: '', err: err}));
         return;
     },
  });

	res.end(JSON.stringify(credentials));
	return;
}

var options = {
  key: fs.readFileSync('/etc/letsencrypt/archive/btapp.threeapps.com.br/privkey1.pem'),
  cert: fs.readFileSync('/etc/letsencrypt/archive/btapp.threeapps.com.br/cert1.pem')
};

https.createServer(options, function (req, res) {

if (req.method === 'POST') {
    let body = '';
    req.on('data', chunk => {
        body += chunk.toString();
    });
    req.on('end', () => {
	var result = JSON.parse(body);
        console.log(result);
        console.log(result.Username);
	signIn(result.Username, result.Password, res);
    });
}


}).listen(3000);




